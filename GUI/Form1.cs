﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using DarkTheme;

namespace pBurningStation
{
    public partial class Form1 : DarkTheme.Form
    {
        #region Form
        public Form1()
        {
            InitializeComponent();

            foreach (string item in pRobot.getCDDrives())
            {
                comboBox1.Items.Add(item);
            }
            comboBox1.SelectedIndex = 0;
            
            button1.AutoEllipsis = true;

            numericUpDown1.BackColor = DarkTheme.Colors.Background;
            numericUpDown1.ForeColor = DarkTheme.Colors.Foreground;
            timer1.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dia = new OpenFileDialog();
            dia.Filter = "DVD-Images |*.iso;*.nrg";
            if (dia.ShowDialog() == DialogResult.OK)
            {
                button1.Text = dia.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Tools().ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            enableControl(false);

            Potato.amount = (int)numericUpDown1.Value;
            Potato.drive = comboBox1.SelectedItem + "";
            Potato.progress = 0;

            Thread burner = new Thread(image);
            burner.Start();
        }


        private void enableControl(bool state)
        {
            button4.Enabled = state;
            comboBox1.Enabled = state;
            button1.Enabled = state;
            numericUpDown1.Enabled = state;
            button2.Enabled = state;
            button3.Enabled = state;
        }

        #endregion


        private void burn()
        {
            pRobot robot = new pRobot();

            int amount = Potato.amount;
            double progress_per_iteration = 100.0 / (double)amount;

            for (int i = 0; i < amount; i++)
            {
                robot.openDrive(Potato.drive);

                robot.SpindelNachBrenner();

                robot.closeDrive(Potato.drive);

                robot.burn(Potato.image);

                robot.openDrive(Potato.drive);

                robot.BrennerNachSpindel();

                Potato.progress += progress_per_iteration;
            }

            enableControl(true);
        }

        private void image()
        {
            pRobot robot = new pRobot();
            bool checkvalue = true;
            while (true)
            {
                checkvalue = robot.SpindelNachBrenner();
                if (!checkvalue)
                    break;
                robot.closeDrive(Potato.drive);
                robot.imagen();
                robot.openDrive(Potato.drive);
                robot.BrennerNachSpindel();
            }

            enableControl(true);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            enableControl(false);

            Potato.amount = (int)numericUpDown1.Value;
            Potato.drive = comboBox1.SelectedItem + "";
            Potato.image = button1.Text;
            Potato.progress = 0;

            FolderBrowserDialog dia = new FolderBrowserDialog();
            if (dia.ShowDialog() == DialogResult.OK)
            {
                Potato.output = dia.SelectedPath;
                Thread burner = new Thread(burn);
                burner.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = (int)(Potato.progress);
            progressLbl.Text = (int)(Potato.progress) + "%";
        }
    }
}
