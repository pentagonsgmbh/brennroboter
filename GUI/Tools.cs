﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DarkTheme;

namespace pBurningStation
{
    public partial class Tools : DarkTheme.Form
    {
        private pRobot r = new pRobot();

        public Tools()
        {
            InitializeComponent();

            foreach (string item in pRobot.getCDDrives())
            {
                comboBox1.Items.Add(item);
                comboBox2.Items.Add(item);
            }
            comboBox1.SelectedIndex = comboBox2.SelectedIndex = 0;
        }

        #region Test-functions
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(r.SpindelNachBrenner() + "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(r.openDrive(comboBox1.SelectedItem + "") + "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(r.Ablegen() + "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(r.closeDrive(comboBox2.SelectedItem + "") + "");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show(r.Aufnehmen() + "");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MessageBox.Show(r.BrennerNachSpindel() + "");
        }
        #endregion

    }
}
