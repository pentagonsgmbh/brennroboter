﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using RobotControl;

namespace pBurningStation
{
    public class pRobot
    {
        private IRobotControl robot = (IRobotControl)new RobotController("COM1");

        #region Robot-Control
        public pRobot()
        {
            //robot.Initialize();
        }

        public bool SpindelNachBrenner()
        {
            bool value = robot.SpindelNachBrenner();
            while (!value)
            {
                value = robot.SpindelNachBrenner();
            }
            return value;
        }

        public bool Ablegen()
        {
            return robot.Ablegen();
        }

        public bool Aufnehmen()
        {
            return robot.Aufnehmen();
        }

        public bool BrennerNachSpindel()
        {
            bool value = robot.BrennerNachSpindel();
            while (!value)
            {
                value = robot.BrennerNachSpindel();
            }
            return value;
        }
        #endregion

        #region CD Drives
        public static List<string> getCDDrives()
        {
            List<string> o = new List<string>();

            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                if (drive.DriveType == DriveType.CDRom)
                    o.Add(drive.Name.Substring(0, 1));
            }

            return o;
        }

        [DllImport("winmm.dll")]
        static extern Int32 mciSendString(String command, StringBuilder buffer, Int32 bufferSize, IntPtr hwndCallback);

        public bool openDrive(string name)
        {
            try
            {
                mciSendString("set CDAudio!" + name + ":\\ door open", null, 0, IntPtr.Zero);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool closeDrive(string name)
        {
            try
            {
                mciSendString("set CDAudio!" + name + ":\\ door closed", null, 0, IntPtr.Zero);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Burning
        public void burn(string image)
        {
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo("ImgBurn.exe /MODE BUILD /BUILDMODE DEVICE /DEST \"" + Potato.drive.ToUpper() + ":\" /SRC \"" + image + "\" /START /CLOSE");
            p.Start();
        }

        public void imagen()
        {
            DateTime a = new DateTime(1970, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan ts = new TimeSpan(now.Ticks - a.Ticks);
            string name = Path.Combine(Potato.output, Convert.ToInt32(ts.TotalSeconds) + ".iso");

            Process p = new Process();
            p.StartInfo = new ProcessStartInfo("ImgBurn.exe /MODE BUILD /OUTPUTMODE IMAGEFILE  /SRC \"" + Potato.drive + "\" /DEST \"image\" /FILESYSTEM \"ISO9660 + UDF\" /UDFREVISION \"1.02\" /VOLUMELABEL_UDF \"" + name + "\" /START /NOIMAGEDETAILS /CLOSE");
            p.Start();
        }
        #endregion
    }
}
