﻿using System;
using System.Collections.Generic;
using System.Text;

using BurnControl;
using RobotControl;

namespace BurnRobot
{
    /// <summary>
    /// Synchronisiert den Roboter und den Brenner.
    /// Stellt das Model im MVC-Pattern da
    /// </summary>
    public class BurnRobotController
    {
        /// <summary>
        /// Wird aufgerufen, wenn der Brenner einen Fortschritt meldet.
        /// </summary>
        public event ProgressUpdatedEventHandler ProgressUpdated;

        private List<BurnTask> _burnTasks;
        public List<BurnTask> BurnTasks { get { return _burnTasks; } }

        private IRobotControl _robotControl;

        /// <summary>
        /// Erzeugt einen neuen BurnRobot
        /// </summary>
        /// <param name="comPort">Der COM-Port an dem der Roboter haengt (z.B. COM1)</param>
        /// <param name="driveLetter">Der Laufwerksbuchstabe des Brenners (z.B. D)</param>
        public BurnRobotController(string comPort)
        {
            _robotControl = new RobotController(comPort);
            _burnTasks = new List<BurnTask>();
        }

        /// <summary>
        /// Fuegt einen Brennauftrag hinzu
        /// </summary>
        /// <param name="burnTask"></param>
        public void AddBurnTask(BurnTask burnTask)
        {
            _burnTasks.Add(burnTask);
        }

        /// <summary>
        /// Entfernt einen Brennauftrag
        /// </summary>
        /// <param name="burnTask"></param>
        public void RemoveBurnTask(BurnTask burnTask)
        {
            _burnTasks.Remove(burnTask);
        }

        /// <summary>
        /// Ruft von jedem BurnController InitBurn() auf, damit diese bereit sind ohne weiteren Input zu brennen.
        /// </summary>
        public void InitBurn()
        {
            foreach (BurnTask burnTask in _burnTasks)
            {
                burnTask.InitBurn();
            }
        }

        /// <summary>
        /// Arbeitet alle BurnController durch und sagt ihnen, sie sollen sich brennen
        /// </summary>
        public void Burn()
        {
            _robotControl.Initialize();

            foreach (BurnTask burnTask in _burnTasks)
            {
                for (int i = 0; i < burnTask.Count; i++)
                {
                    // Ausgangsposition:
                    // Roboterarm in der Mitte,
                    // Keine Disk im Laufwerk,
                    // Alle Rohlinge auf der Spindel,
                    // Laufwerk eingefahren
                    if (!burnTask.EjectDisc())
                        throw new Exception("Konnte Disk nicht auswerfen!");
                    if (!_robotControl.SpindelNachBrenner())
                        throw new Exception("Spindeldisk konnte nicht zum Brenner bewegt werden:\n" + _robotControl.LastError);
                    if (!burnTask.CloseTray())
                        throw new Exception("Laufwerk konnte nicht eingefahren werden!");
                    if (!_robotControl.Initialize())
                        throw new Exception("Roboter konnte nicht initialisiert werden!");
                    burnTask.Burn(); //Burn kuemmert sich selbst um die Exceptions
                    if (!burnTask.EjectDisc())
                        throw new Exception("Konnte Disk nicht auswerfen!");
                    if (!_robotControl.BrennerNachSpindel())
                        throw new Exception("Brennerdisk konnte nicht zur Spindel bewegt werden!");
                    if (!burnTask.CloseTray())
                        throw new Exception("Laufwerk konnte nicht eingefahren werden!");
                    if (!_robotControl.Initialize())
                        throw new Exception("Roboter konnte nicht initialisiert werden!");
                }
            }
        }
    }
}