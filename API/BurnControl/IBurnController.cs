﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BurnControl
{
    public interface IBurnController
    {
        /// <summary>
        /// Bereitet alles fuer das Brennen mit Burn vor.
        /// Zeigt z.B. einen Dialog an, wo man ein Image speichern will und erstellt dieses dann auch.
        /// </summary>
        /// <returns>Ob der Brennvorgang initialisiert werden konnte.</returns>
        bool InitBurn();

        /// <summary>
        /// Brennt anhand der in BurnInit gewaehlten Einstellungen eine Disk.
        /// </summary>
        /// <param name="burnMedia"></param>
        /// <returns>Ob der Brennvorgang erfolgreich war.</returns>
        bool Burn();

        /// <summary>
        /// Schmeisst das Laufwerk aus.
        /// </summary>
        /// <returns>Ob der Vorgang erfolgreich war.</returns>
        bool EjectDisc();

        /// <summary>
        /// Schliesst das Laufwerk wieder.
        /// </summary>
        /// <returns>Ob der Vorgang erfolgreich war.</returns>
        bool CloseTray();

        /// <summary>
        /// Dieses Event wird immer gefeuert, wenn beim Brennen ein Fortschritt erzielt wurde.
        /// So kann immer ein Statusbalken waehrend des Brennens angezeigt werden.
        /// </summary>
        event ProgressUpdatedEventHandler ProgressUpdated;
    }
}
