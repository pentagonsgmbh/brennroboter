﻿using System;
using System.Collections.Generic;
using System.Text;
using NEROLib;
using System.Threading;

namespace BurnControl
{
    public abstract class NeroBurnController : IBurnController
    {
        protected Nero _nero;
        protected NeroDrive _drive;
        protected INeroCDInfo _discInfo;

        public NeroBurnController()
        {
            _nero = new Nero();
            NeroDrives drives = _nero.GetDrives(NERO_MEDIA_TYPE.NERO_MEDIA_NONE);

            for (int i = 0; i < drives.Count; i++)
			{
                if (drives.Item(i).DeviceName == "Optiarc  DVD RW AD-5170A ")
                    _drive = (NeroDrive)drives.Item(i);
			}

            _drive.OnDoneCDInfo += new _INeroDriveEvents_OnDoneCDInfoEventHandler(_drive_OnDoneCDInfo);
        }

        void _drive_OnDoneCDInfo(INeroCDInfo pCDInfo)
        {
            _discInfo = pCDInfo;
        }

        /// <summary>
        /// Bereitet alles fuer das Brennen mit Burn vor.
        /// Zeigt z.B. einen Dialog an, wo man ein Image speichern will und erstellt dieses dann auch.
        /// </summary>
        /// <returns>
        /// Ob der Brennvorgang initialisiert werden konnte.
        /// </returns>
        public abstract bool InitBurn();

        /// <summary>
        /// Brennt anhand der in BurnInit gewaehlten Einstellungen eine Disk.
        /// </summary>
        /// <returns>Ob der Brennvorgang erfolgreich war.</returns>
        public abstract bool Burn();

        /// <summary>
        /// Schmeisst das Laufwerk aus.
        /// </summary>
        /// <returns>Ob der Vorgang erfolgreich war.</returns>
        public bool EjectDisc()
        {
            try
            {
                _drive.EjectCD();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Schliesst das Laufwerk wieder.
        /// </summary>
        /// <returns>Ob der Vorgang erfolgreich war.</returns>
        public bool CloseTray()
        {
            try
            {
                _drive.LoadCD();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Loescht die aktuell eingelegte CD-RW, bzw DVD-RW.
        /// </summary>
        /// <returns>
        /// False, wenn keine RW eingelegt ist, oder ein anderer Fehler aufgetreten ist.
        /// </returns>
        public bool EraseDisc()
        {
            try
            {
                _drive.EraseDisc(true, NERO_ERASE_MODE.NERO_ERASE_MODE_DISABLE_EJECT);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        protected void OnProgressUpdated(string status, float progress)
        {
            if (ProgressUpdated != null)
                ProgressUpdated(this, new ProgressUpdatedEventArgs(status, progress));
        }

        /// <summary>
        /// Gets the max write speed.
        /// </summary>
        /// <returns></returns>
        protected int GetMaxWriteSpeed()
        {
            return 0;
        }

        /// <summary>
        /// Gets the max read speed.
        /// </summary>
        /// <returns></returns>
        protected int GetMaxReadSpeed()
        {
            return 0;
        }

        /// <summary>
        /// Gets the current media.
        /// </summary>
        /// <returns></returns>
        protected NERO_MEDIA_TYPE GetCurrentMedia()
        {
            AutoResetEvent are = new AutoResetEvent(false);
            _drive.OnDoneCDInfo += delegate { are.Set(); };
            _drive.CDInfo(NERO_CDINFO_FLAGS.NERO_READ_CD_TEXT);
            are.WaitOne();
            return _discInfo.MediaType;
        }

        /// <summary>
        /// Gets the image recorder.
        /// </summary>
        /// <returns></returns>
        protected NeroDrive GetImageRecorder()
        {
            NeroDrives drives = _nero.GetDrives(NERO_MEDIA_TYPE.NERO_MEDIA_NONE);
            for (int i = 0; i < drives.Count; i++)
            {
                NeroDrive drive = (NeroDrive)drives.Item(i);
                if (drive.DeviceName == "Image Recorder")
                    return drive;
            }
            throw new Exception("Image Recorder not found!");
        }

        /// <summary>
        /// Dieses Event wird immer gefeuert, wenn beim Brennen ein Fortschritt erzielt wurde.
        /// So kann immer ein Statusbalken waehrend des Brennens angezeigt werden.
        /// </summary>
        public event ProgressUpdatedEventHandler ProgressUpdated;
    }
}
