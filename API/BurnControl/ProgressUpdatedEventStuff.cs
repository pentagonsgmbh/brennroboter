﻿/* 
 * Dieses File beinhaltet EventHandler und EventArgs fuer das ProgressUpdatedEvent.
 * */

using System;
using System.Collections.Generic;
using System.Text;

namespace BurnControl
{
    public delegate void ProgressUpdatedEventHandler(object sender, ProgressUpdatedEventArgs e);
    public class ProgressUpdatedEventArgs
    {
        public float Progress { get; set; }
        public string Status { get; set; }

        public ProgressUpdatedEventArgs(string status, float progress)
        {
            Progress = progress;
            Status = status;
        }
    }
}
