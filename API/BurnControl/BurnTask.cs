﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BurnControl
{
    /// <summary>
    /// Enthaelt Informationen darueber, wie eine Disk gebrannt wird.
    /// </summary>
    public class BurnTask
    {
        /// <summary>
        /// Gibt den BurnController, der die Brennlogik enthaelt an.
        /// </summary>
        public IBurnController BurnController { get { return _burnController; } }
        private IBurnController _burnController;

        /// <summary>
        /// Gibt an, wieviele Disks gebrannt werden sollen.
        /// </summary>
        public int Count { get { return _count; } }
        private int _count;

        /// <summary>
        /// Gibt an, ob der BurnController erfolgreich initialisiert wurde
        /// </summary>
        public bool Initialized { get { return _initialized; } }
        private bool _initialized = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="burnController">Der BurnController der zum Brennen benutzt wird</param>
        /// <param name="count">Die Anzahl an Disks, die damit gebrannt werden soll</param>
        public BurnTask(IBurnController burnController, int count)
        {
            _burnController = burnController;
            _count = count;
        }

        /// <summary>
        /// Initialisiert den BurnController
        /// </summary>
        public void InitBurn()
        {
            _initialized = _burnController.InitBurn();
        }

        /// <summary>
        /// Brennt EINE Disk.
        /// </summary>
        public bool Burn()
        {
            if (!Initialized)
                return false;

            return _burnController.Burn();
        }

        public bool EjectDisc()
        {
            return _burnController.EjectDisc();
        }

        public bool CloseTray()
        {
            return _burnController.CloseTray();
        }
    }
}
