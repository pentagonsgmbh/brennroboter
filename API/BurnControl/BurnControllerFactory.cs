﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BurnControl
{
    public class BurnControllerFactory
    {
        //=================SINGLETON======================
        private static BurnControllerFactory _factory;
        public static BurnControllerFactory Factory
        {
            get
            {
                if (_factory == null)
                    _factory = new BurnControllerFactory();
                return _factory;
            }
        }
        //================================================

        private BurnControllerFactory()
        {
        }

        public IBurnController GetBurner(BurnControllerEnum burnController)
        {
            switch (burnController)
            {
                case BurnControllerEnum.ImageBurner:
                    return new ImageBurner();
                default:
                    throw new Exception("BurnController nicht implementiert");
            }
        }
    }
}