﻿using System;
using System.Collections.Generic;
using System.Text;
using NEROLib;
using System.Windows.Forms;

namespace BurnControl
{
    /// <summary>
    /// Konkreter BurnController, der fuer das Brennen von ISO-Images zustaendig ist.
    /// </summary>
    public class ImageBurner : NeroBurnController
    {
        /// <summary>
        /// Gibt an, wo das nrg-image gespeichert wurde
        /// </summary>
        string _imagePath;

        public ImageBurner()
            : base()
        {
        }

        public override bool InitBurn()
        {
            EjectDisc();
            if (MessageBox.Show("Legen sie die zu brennende Disk ein", "Disk einlegen", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                return false;
            CloseTray();

            NeroFiddlesCOM.NET.BurnProgressForm form = new NeroFiddlesCOM.NET.BurnProgressForm(GetImageRecorder(), _nero);
            form.BurnDiscCopy(_drive, false, true, GetMaxReadSpeed(), 1, false, true, false, false, NERO_BURN_FLAGS.NERO_BURN_FLAG_WRITE | NERO_BURN_FLAGS.NERO_BURN_FLAG_DISABLE_ABORT | NERO_BURN_FLAGS.NERO_BURN_FLAG_DISABLE_EJECT | NERO_BURN_FLAGS.NERO_BURN_FLAG_CD_TEXT | NERO_BURN_FLAGS.NERO_BURN_FLAG_CLOSE_SESSION | NERO_BURN_FLAGS.NERO_BURN_FLAG_BUF_UNDERRUN_PROT | NERO_BURN_FLAGS.NERO_BURN_FLAG_DAO, GetMaxWriteSpeed(), GetCurrentMedia(), null);
            _imagePath = form.ImagePath;

            EjectDisc();
            MessageBox.Show("Das Image wurde erfolgreich erstellt.\nEntfernen sie jetzt bitte die Disk.\nDruecken sie OK, sobald sie die Disk entfernt haben!", "Image erstellt", MessageBoxButtons.OK);
            CloseTray();

            return form.DialogResult == System.Windows.Forms.DialogResult.OK;
        }

        public override bool Burn()
        {
            try
            {
                //_drive.BurnImage2(_imagePath, NERO_BURN_FLAGS.NERO_BURN_FLAG_WRITE | NERO_BURN_FLAGS.NERO_BURN_FLAG_DISABLE_EJECT | NERO_BURN_FLAGS.NERO_BURN_FLAG_CLOSE_SESSION, GetMaxWriteSpeed(), GetCurrentMedia());
                NeroFiddlesCOM.NET.BurnProgressForm form = new NeroFiddlesCOM.NET.BurnProgressForm(_drive, _nero);
                form.BurnImage(_imagePath, NERO_BURN_FLAGS.NERO_BURN_FLAG_WRITE | NERO_BURN_FLAGS.NERO_BURN_FLAG_DISABLE_EJECT, 0, GetCurrentMedia(), null);
                return true;
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter("errors.txt", true))
                {
                    writer.WriteLine(ex.ToString());
                    writer.WriteLine("\n\n\n");
                }
                return false;
            }
        }
    }
}
