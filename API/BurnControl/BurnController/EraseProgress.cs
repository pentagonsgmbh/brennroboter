using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using NEROLib;

namespace NeroFiddlesCOM.NET
{
	/// <summary>
	/// Summary description for EraseProgress.
	/// </summary>
	public class EraseProgressForm : System.Windows.Forms.Form
	{
		private NeroDrive m_drive;
		private bool m_bQuick;
		private bool m_bShowResultDialog;

		private DateTime m_timeStart;
		private System.Timers.Timer m_timer;
		private int m_iErasingTime;

		private _INeroDriveEvents_OnDoneEraseEventHandler m_evOnDoneErase;

		private System.Windows.Forms.ProgressBar c_Progress;
		private System.Windows.Forms.Label c_ProgressPercent;
		private System.Windows.Forms.Label c_Caption;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public EraseProgressForm(NeroDrive drive, bool bQuick)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			m_drive = drive;
			m_bQuick = bQuick;
			m_bShowResultDialog = true;
		}

		public EraseProgressForm(NeroDrive drive, bool bQuick, bool bShowResultDialog)
			: this (drive, bQuick)
		{
			m_bShowResultDialog = bShowResultDialog;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.c_Progress = new System.Windows.Forms.ProgressBar();
			this.c_ProgressPercent = new System.Windows.Forms.Label();
			this.c_Caption = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// c_Progress
			// 
			this.c_Progress.Location = new System.Drawing.Point(8, 56);
			this.c_Progress.Name = "c_Progress";
			this.c_Progress.Size = new System.Drawing.Size(320, 23);
			this.c_Progress.TabIndex = 2;
			// 
			// c_ProgressPercent
			// 
			this.c_ProgressPercent.Location = new System.Drawing.Point(144, 32);
			this.c_ProgressPercent.Name = "c_ProgressPercent";
			this.c_ProgressPercent.Size = new System.Drawing.Size(49, 16);
			this.c_ProgressPercent.TabIndex = 1;
			this.c_ProgressPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// c_Caption
			// 
			this.c_Caption.Location = new System.Drawing.Point(8, 8);
			this.c_Caption.Name = "c_Caption";
			this.c_Caption.Size = new System.Drawing.Size(320, 16);
			this.c_Caption.TabIndex = 0;
			this.c_Caption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// EraseProgressForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(338, 96);
			this.ControlBox = false;
			this.Controls.Add(this.c_Caption);
			this.Controls.Add(this.c_ProgressPercent);
			this.Controls.Add(this.c_Progress);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "EraseProgressForm";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "EraseProgress";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			c_Caption.Text = "Erasing " + m_drive.DeviceName;

			// Get the erasing time for the erasing method chosen.
			// We will use this time as an estimate of the process
			// duration.
			// 
			m_iErasingTime = m_drive.get_CDRWErasingTime (m_bQuick);

			// We will now start a timer that will be used for updating
			// the elapsed time.
			// 
			m_timeStart = System.DateTime.Now;
			m_timer = new System.Timers.Timer (500);
			m_timer.Elapsed += new System.Timers.ElapsedEventHandler(m_timer_Elapsed);
			m_timer.Start ();

			// Subscribe to the OnDoneErase event.
			// 
			m_evOnDoneErase = new _INeroDriveEvents_OnDoneEraseEventHandler(m_drive_OnDoneErase);
			m_drive.OnDoneErase += m_evOnDoneErase;

			// Finally, start the erase process.
			// 
			m_drive.EraseDisc (m_bQuick, NERO_ERASE_MODE.NERO_ERASE_MODE_DEFAULT);
		}

		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed (e);

			// When the form is closed, make sure to unsubscribe from
			// the event.
			// 
			m_drive.OnDoneErase -= m_evOnDoneErase;
		}

		private void m_drive_OnDoneErase(ref bool Ok)
		{
			// When the erase process is done, let's stop
			// the timer.
			// 
			m_timer.Stop ();

			// If we are required to show the result dialog,
			// this is the time and place to do it.
			// 
			if (m_bShowResultDialog)
			{
				// First hide the progress form, then show the success
				// or failure.
				// 
				this.Hide ();
				MessageBox.Show (this, "Erase operation was " + (Ok? "successful!": "unsuccessful!"));
			}

			// Auto close the dialog when the erase is finished.
			// 
			this.Close ();
		}

		private void m_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
            if (c_Progress.InvokeRequired)
            {
                c_Progress.Invoke(new System.Timers.ElapsedEventHandler(m_timer_Elapsed));
            }


			// This method is called when the timer period elapses.
			// We should update the elapsed time here.
			// 
			TimeSpan ts = System.DateTime.Now - m_timeStart;
			int iElapsedTime = ts.Minutes*60 + ts.Seconds;
			int iPercent = (iElapsedTime*100)/m_iErasingTime;

			c_ProgressPercent.Text = iPercent.ToString () + "%";
			c_Progress.Value = iPercent;
		}
	}
}
