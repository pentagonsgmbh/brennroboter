using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace NeroFiddlesCOM.NET
{
	/// <summary>
	/// Summary description for WaitCD.
	/// </summary>
	public class WaitCDForm : System.Windows.Forms.Form
	{
		// This is our own event. It will be called when our
		// Cancel button is clicked.
		// 
		public event EventHandler OnCancel;

		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.TextBox c_LastDetectedMedia;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.TextBox c_RequestedMedia;
		private System.Windows.Forms.Button c_Cancel;
		public System.Windows.Forms.Label c_WaitCDText;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public WaitCDForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.c_LastDetectedMedia = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.c_RequestedMedia = new System.Windows.Forms.TextBox();
			this.c_Cancel = new System.Windows.Forms.Button();
			this.c_WaitCDText = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Last detected media:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// c_LastDetectedMedia
			// 
			this.c_LastDetectedMedia.Location = new System.Drawing.Point(112, 72);
			this.c_LastDetectedMedia.Name = "c_LastDetectedMedia";
			this.c_LastDetectedMedia.ReadOnly = true;
			this.c_LastDetectedMedia.Size = new System.Drawing.Size(176, 20);
			this.c_LastDetectedMedia.TabIndex = 2;
			this.c_LastDetectedMedia.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 96);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(104, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Requested media:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// c_RequestedMedia
			// 
			this.c_RequestedMedia.Location = new System.Drawing.Point(112, 96);
			this.c_RequestedMedia.Name = "c_RequestedMedia";
			this.c_RequestedMedia.ReadOnly = true;
			this.c_RequestedMedia.Size = new System.Drawing.Size(176, 20);
			this.c_RequestedMedia.TabIndex = 4;
			this.c_RequestedMedia.Text = "";
			// 
			// c_Cancel
			// 
			this.c_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.c_Cancel.Location = new System.Drawing.Point(112, 128);
			this.c_Cancel.Name = "c_Cancel";
			this.c_Cancel.TabIndex = 5;
			this.c_Cancel.Text = "Cancel";
			this.c_Cancel.Click += new System.EventHandler(this.c_Cancel_Click);
			// 
			// c_WaitCDText
			// 
			this.c_WaitCDText.Location = new System.Drawing.Point(8, 8);
			this.c_WaitCDText.Name = "c_WaitCDText";
			this.c_WaitCDText.Size = new System.Drawing.Size(280, 56);
			this.c_WaitCDText.TabIndex = 0;
			// 
			// WaitCDForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.c_Cancel;
			this.ClientSize = new System.Drawing.Size(298, 160);
			this.ControlBox = false;
			this.Controls.Add(this.c_WaitCDText);
			this.Controls.Add(this.c_Cancel);
			this.Controls.Add(this.c_LastDetectedMedia);
			this.Controls.Add(this.c_RequestedMedia);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "WaitCDForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "WaitCD";
			this.ResumeLayout(false);

		}
		#endregion

		private void c_Cancel_Click(object sender, System.EventArgs e)
		{
			// If there is an event subscriber, let's notify it.
			// 
			if (null != OnCancel)
			{
				OnCancel (this, null);
			}
		}
	}
}
