using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using NEROLib;

namespace NeroFiddlesCOM.NET
{
	/// <summary>
	/// Summary description for BurnProgress.
	/// </summary>
	public class BurnProgressForm : System.Windows.Forms.Form
	{
        public string ImagePath { get; set; }

		private NeroDrive m_drive;
		private Nero m_nero;

		private DateTime m_timeStart;
		private System.Timers.Timer m_timer;
		private WaitCDForm m_frmWaitCD;

		private bool m_bAborted;

		private _INeroDriveEvents_OnDoneBurnEventHandler m_evOnDoneBurn;
		private _INeroDriveEvents_OnProgressEventHandler m_evOnProgress;
		private _INeroDriveEvents_OnSubTaskProgressEventHandler m_evOnSubTaskProgress;
		private _INeroDriveEvents_OnAbortedEventHandler m_evOnAborted;
		private _INeroDriveEvents_OnAddLogLineEventHandler m_evOnAddLogLine;
		private _INeroDriveEvents_OnSetPhaseEventHandler m_evOnSetPhase;
		private _INeroDriveEvents_OnDisableAbortEventHandler m_evOnDisableAbort;
		private _INeroEvents_OnFileSelImageEventHandler m_evOnFileSelImage;
		private _INeroEvents_OnWaitCDEventHandler m_evOnWaitCD;
		private _INeroEvents_OnWaitCDDoneEventHandler m_evOnWaitCDDone;
		private _INeroEvents_OnWaitCDMediaInfoEventHandler m_evOnWaitCDMediaInfo;
		private _INeroEvents_OnNonEmptyCDRWEventHandler m_evOnNonEmptyCDRW;

		private System.Windows.Forms.ListView c_Events;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label c_TimeElapsed;
		private System.Windows.Forms.ProgressBar c_SubTask;
		private System.Windows.Forms.ProgressBar c_Task;
		private System.Windows.Forms.Label c_SubTaskPercent;
		private System.Windows.Forms.Label c_TaskPercent;
		private System.Windows.Forms.Button c_Done;
		private System.Windows.Forms.Button c_Cancel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label c_Phase;
		private System.Windows.Forms.Button c_SaveLog;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public BurnProgressForm(NeroDrive drive, Nero nero)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			m_drive = drive;
			m_nero = nero;
			m_bAborted = false;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// Upon loading, create a new WaitCD form and subscribe to its
			// OnCancel event that we will use to abort the burning process.
			// 
			m_frmWaitCD = new WaitCDForm ();
			m_frmWaitCD.OnCancel += new EventHandler(m_frmWaitCD_OnCancel);
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.c_Events = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.label1 = new System.Windows.Forms.Label();
			this.c_TimeElapsed = new System.Windows.Forms.Label();
			this.c_SubTask = new System.Windows.Forms.ProgressBar();
			this.c_Task = new System.Windows.Forms.ProgressBar();
			this.c_SubTaskPercent = new System.Windows.Forms.Label();
			this.c_TaskPercent = new System.Windows.Forms.Label();
			this.c_Done = new System.Windows.Forms.Button();
			this.c_Cancel = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.c_Phase = new System.Windows.Forms.Label();
			this.c_SaveLog = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// c_Events
			// 
			this.c_Events.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					   this.columnHeader1,
																					   this.columnHeader2});
			this.c_Events.Location = new System.Drawing.Point(8, 8);
			this.c_Events.Name = "c_Events";
			this.c_Events.Size = new System.Drawing.Size(552, 168);
			this.c_Events.TabIndex = 8;
			this.c_Events.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Time";
			this.columnHeader1.Width = 69;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Events";
			this.columnHeader2.Width = 458;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(392, 184);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Time elapsed:";
			// 
			// c_TimeElapsed
			// 
			this.c_TimeElapsed.Location = new System.Drawing.Point(480, 184);
			this.c_TimeElapsed.Name = "c_TimeElapsed";
			this.c_TimeElapsed.Size = new System.Drawing.Size(80, 16);
			this.c_TimeElapsed.TabIndex = 3;
			this.c_TimeElapsed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// c_SubTask
			// 
			this.c_SubTask.Location = new System.Drawing.Point(48, 208);
			this.c_SubTask.Name = "c_SubTask";
			this.c_SubTask.Size = new System.Drawing.Size(512, 23);
			this.c_SubTask.TabIndex = 5;
			// 
			// c_Task
			// 
			this.c_Task.Location = new System.Drawing.Point(48, 240);
			this.c_Task.Name = "c_Task";
			this.c_Task.Size = new System.Drawing.Size(512, 23);
			this.c_Task.TabIndex = 7;
			// 
			// c_SubTaskPercent
			// 
			this.c_SubTaskPercent.Location = new System.Drawing.Point(8, 208);
			this.c_SubTaskPercent.Name = "c_SubTaskPercent";
			this.c_SubTaskPercent.Size = new System.Drawing.Size(40, 24);
			this.c_SubTaskPercent.TabIndex = 4;
			this.c_SubTaskPercent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// c_TaskPercent
			// 
			this.c_TaskPercent.Location = new System.Drawing.Point(8, 240);
			this.c_TaskPercent.Name = "c_TaskPercent";
			this.c_TaskPercent.Size = new System.Drawing.Size(40, 24);
			this.c_TaskPercent.TabIndex = 6;
			this.c_TaskPercent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// c_Done
			// 
			this.c_Done.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.c_Done.Enabled = false;
			this.c_Done.Location = new System.Drawing.Point(408, 272);
			this.c_Done.Name = "c_Done";
			this.c_Done.TabIndex = 10;
			this.c_Done.Text = "Done";
			// 
			// c_Cancel
			// 
			this.c_Cancel.Location = new System.Drawing.Point(488, 272);
			this.c_Cancel.Name = "c_Cancel";
			this.c_Cancel.TabIndex = 11;
			this.c_Cancel.Text = "Cancel";
			this.c_Cancel.Click += new System.EventHandler(this.c_Cancel_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 184);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 16);
			this.label2.TabIndex = 0;
			this.label2.Text = "Phase:";
			// 
			// c_Phase
			// 
			this.c_Phase.Location = new System.Drawing.Point(56, 184);
			this.c_Phase.Name = "c_Phase";
			this.c_Phase.Size = new System.Drawing.Size(320, 16);
			this.c_Phase.TabIndex = 1;
			// 
			// c_SaveLog
			// 
			this.c_SaveLog.Enabled = false;
			this.c_SaveLog.Location = new System.Drawing.Point(328, 272);
			this.c_SaveLog.Name = "c_SaveLog";
			this.c_SaveLog.TabIndex = 9;
			this.c_SaveLog.Text = "Save Log";
			this.c_SaveLog.Click += new System.EventHandler(this.c_SaveLog_Click);
			// 
			// BurnProgressForm
			// 
			this.AcceptButton = this.c_Done;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(568, 302);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.c_Done);
			this.Controls.Add(this.c_SubTask);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.c_Events);
			this.Controls.Add(this.c_TimeElapsed);
			this.Controls.Add(this.c_Task);
			this.Controls.Add(this.c_SubTaskPercent);
			this.Controls.Add(this.c_TaskPercent);
			this.Controls.Add(this.c_Cancel);
			this.Controls.Add(this.c_Phase);
			this.Controls.Add(this.c_SaveLog);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "BurnProgressForm";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "BurnProgress";
			this.ResumeLayout(false);

		}
		#endregion

		// This is the function for burning an image.
		// 
		public void BurnImage (string sImageFilename, NERO_BURN_FLAGS burnFlags, int iSpeedInKBS, NERO_MEDIA_TYPE mediaType, System.Windows.Forms.IWin32Window owner)
		{
			try
			{
				// First subscribe to all events of interest.
				// 
				SubscribeToEvents (true);

				// Now, start burning.
				// 
				m_drive.BurnImage2 (sImageFilename,
					(NERO_BURN_FLAGS) ((uint) burnFlags | (uint) NERO_BURN_FLAGS.NERO_BURN_FLAG_SPEED_IN_KBS),
					iSpeedInKBS,
					mediaType);

				this.ShowDialog (owner);
			}
			catch (COMException ex)
			{
				SubscribeToEvents (false);

				MessageBox.Show (this, ex.Message);
			}
		}

		public void BurnIsoAudio (string sArtist, string sTitle, NeroISOTrack isoTrack, NeroAudioTracks audioTracks, NeroCDStamp cdStamp, NERO_BURN_FLAGS nbf, int iSpeedInKBS, NERO_MEDIA_TYPE mediaType, System.Windows.Forms.IWin32Window owner)
		{
			try
			{
				// First subscribe to all events of interest.
				// 
				SubscribeToEvents (true);

				// If the iso track passed in has neither files nor folders,
				// don't use it.
				// 
				if (isoTrack.RootFolder.Files.Count == 0 &&
					isoTrack.RootFolder.Folders.Count == 0)
				{
					isoTrack = null;
				}

				// Just pass on all the gathered parameters.
				// 
				m_drive.BurnIsoAudioCD (sArtist, sTitle, false,
					isoTrack, audioTracks, cdStamp,
					(NERO_BURN_FLAGS) ((uint) nbf | (uint) NERO_BURN_FLAGS.NERO_BURN_FLAG_SPEED_IN_KBS),
					iSpeedInKBS,
					mediaType);

				this.ShowDialog (owner);
			}
			catch (COMException ex)
			{
				SubscribeToEvents (false);

				MessageBox.Show (this, ex.Message);
			}
		}

		private void m_drive_OnDoneBurn(ref NERO_BURN_ERROR StatusCode)
		{
			// When burning is over, make sure to unsubscribe from all
			// events.
			// 
			SubscribeToEvents (false);

			// Show the appropriate message for success and failure.
			// 

            DialogResult = DialogResult.OK;

			// Disable the Cancel button and enable the Done
			// button.
			// 
			c_Cancel.Enabled = false;
			c_Done.Enabled = true;
			c_SaveLog.Enabled = true;

            this.Close();
		}

		private void c_Cancel_Click(object sender, System.EventArgs e)
		{
			// When a Cancel button is clicked, make sure to abort.
			// 
			m_bAborted = true;
			m_nero.Abort ();
		}

		private void SubscribeToEvents (bool bSubscribe)
		{
			if (bSubscribe)
			{
				// Subscribe to NeroDrive events.
				// 
				m_evOnDoneBurn = new _INeroDriveEvents_OnDoneBurnEventHandler(m_drive_OnDoneBurn);
				m_drive.OnDoneBurn += m_evOnDoneBurn;
				m_evOnProgress = new _INeroDriveEvents_OnProgressEventHandler(m_drive_OnProgress);
				m_drive.OnProgress += m_evOnProgress;
				m_evOnSubTaskProgress = new _INeroDriveEvents_OnSubTaskProgressEventHandler(m_drive_OnSubTaskProgress);
				m_drive.OnSubTaskProgress += m_evOnSubTaskProgress;
				m_evOnAborted = new _INeroDriveEvents_OnAbortedEventHandler(m_drive_OnAborted);
				m_drive.OnAborted += m_evOnAborted;
				m_evOnAddLogLine = new _INeroDriveEvents_OnAddLogLineEventHandler(m_drive_OnAddLogLine);
				m_drive.OnAddLogLine += m_evOnAddLogLine;
				m_evOnSetPhase = new _INeroDriveEvents_OnSetPhaseEventHandler(m_drive_OnSetPhase);
				m_drive.OnSetPhase += m_evOnSetPhase;
				m_evOnDisableAbort = new _INeroDriveEvents_OnDisableAbortEventHandler(m_drive_OnDisableAbort);
				m_drive.OnDisableAbort += m_evOnDisableAbort;

				// Subscribe to Nero events.
				// 
				m_evOnFileSelImage = new _INeroEvents_OnFileSelImageEventHandler(m_nero_OnFileSelImage);
				m_nero.OnFileSelImage += m_evOnFileSelImage;
				m_evOnWaitCD = new _INeroEvents_OnWaitCDEventHandler(m_nero_OnWaitCD);
				m_nero.OnWaitCD += m_evOnWaitCD;
				m_evOnWaitCDDone = new _INeroEvents_OnWaitCDDoneEventHandler(m_nero_OnWaitCDDone);
				m_nero.OnWaitCDDone += m_evOnWaitCDDone;
				m_evOnWaitCDMediaInfo = new _INeroEvents_OnWaitCDMediaInfoEventHandler(m_nero_OnWaitCDMediaInfo);
				m_nero.OnWaitCDMediaInfo += m_evOnWaitCDMediaInfo;
				m_evOnNonEmptyCDRW = new _INeroEvents_OnNonEmptyCDRWEventHandler(m_nero_OnNonEmptyCDRW);
				m_nero.OnNonEmptyCDRW += m_evOnNonEmptyCDRW;

				// Start the timer that is responsible for displaying
				// the elapsed time during burning.
				// 
				m_timeStart = DateTime.Now;
				m_timer = new System.Timers.Timer (500);
				m_timer.Elapsed += new System.Timers.ElapsedEventHandler(m_timer_Elapsed);
				m_timer.Start ();
			}
			else
			{
				// Stop the elapsed time timer.
				// 
				m_timer.Stop ();

				// Unsubscribe from NeroDrive events.
				// 
				m_drive.OnDoneBurn -= m_evOnDoneBurn;
				m_drive.OnProgress -= m_evOnProgress;
				m_drive.OnSubTaskProgress -= m_evOnSubTaskProgress;
				m_drive.OnAborted -= m_evOnAborted;
				m_drive.OnAddLogLine -= m_evOnAddLogLine;
				m_drive.OnSetPhase -= m_evOnSetPhase;
				m_drive.OnDisableAbort -= m_evOnDisableAbort;

				// Unsubscribe from Nero events.
				// 
				m_nero.OnFileSelImage -= m_evOnFileSelImage;
				m_nero.OnWaitCD -= m_evOnWaitCD;
				m_nero.OnWaitCDDone -= m_evOnWaitCDDone;
				m_nero.OnWaitCDMediaInfo -= m_evOnWaitCDMediaInfo;
				m_nero.OnNonEmptyCDRW -= m_evOnNonEmptyCDRW;
			}
		}

		private void m_nero_OnFileSelImage(ref string Filename)
		{
            Filename = "";
            DialogResult = DialogResult.OK;
		}

		private void m_drive_OnProgress(ref int ProgressInPercent, ref bool Abort)
		{
			// This events gives us an opportunity to show progress
			// as well as abort if needed.
			// 
			Abort = m_bAborted;

			c_Task.Value = ProgressInPercent;
			c_TaskPercent.Text = ProgressInPercent.ToString () + "%";
		}

		private void m_drive_OnSubTaskProgress(ref int ProgressInPercent, ref bool Abort)
		{
			// This event gives us the other task's progress.
			// 
			Abort = m_bAborted;

			c_SubTask.Value = ProgressInPercent;
			c_SubTaskPercent.Text = ProgressInPercent.ToString () + "%";
		}

		private void m_drive_OnAborted(ref bool Abort)
		{
			// This is a general aborted event that is called
			// more frequently than other events that might
			// also give us a chance to abort.
			// 
			Abort = m_bAborted;
		}

		private void m_drive_OnAddLogLine(ref NERO_TEXT_TYPE TextType, ref string Text)
		{
			// This one informs us of the important events during
			// the burn process. We simply add a line to the list
			// view with the current time beside it.
			// 
			DateTime time = DateTime.Now;
			
			ListViewItem lvi = c_Events.Items.Add (time.ToLongTimeString ());
			lvi.SubItems.Add (Text);
		}

		private void m_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			// When the timer ticks, we should update the elapsed
			// time.
			// 
			TimeSpan ts = System.DateTime.Now - m_timeStart;

            if (c_TimeElapsed.InvokeRequired)
            {
                c_TimeElapsed.Invoke(new System.Timers.ElapsedEventHandler(m_timer_Elapsed), sender, e);
                return;
            }
			c_TimeElapsed.Text = ts.Hours.ToString ("00") + ":" + ts.Minutes.ToString ("00") + ":" + ts.Seconds.ToString ("00");
		}

		private void m_drive_OnSetPhase(ref string Text)
		{
			// When a new phase comes in, display it.
			// 
			c_Phase.Text = Text;
		}

		private void m_nero_OnWaitCD(ref NERO_WAITCD_TYPE WaitCD, ref string WaitCDLocalizedText)
		{
			// Ok, so now we need to wait on a disc. Let's pass
			// the WaitCD form the text and show it (if not already
			// shown). Make sure to disable the parent (this window)
			// so that a modal behavior is simulated.
			// 
			m_frmWaitCD.c_WaitCDText.Text = WaitCDLocalizedText;
			m_frmWaitCD.Owner = this;
			m_frmWaitCD.Show ();
			this.Enabled = false;
		}

		private void m_nero_OnWaitCDDone()
		{
			// When waiting on a disc is done, make sure to
			// enable us and hide the WaitCD form.
			// 
			this.Enabled = true;
			m_frmWaitCD.Hide ();
		}

		private void m_nero_OnWaitCDMediaInfo(ref NERO_MEDIA_TYPE LastDetectedMedia, ref string LastDetectedMediaName, ref NERO_MEDIA_TYPE RequestedMedia, ref string RequestedMediaName)
		{
			// When this event is fired, we need to update the
			// information on the WaitCD form. Also, sometimes
			// this event can come before the actual OnWaitCD so
			// make sure that either of them is prepared to
			// show the WaitCD form.
			// 
			m_frmWaitCD.c_LastDetectedMedia.Text = LastDetectedMediaName;
			m_frmWaitCD.c_RequestedMedia.Text = RequestedMediaName;
			m_frmWaitCD.Owner = this;
			m_frmWaitCD.Show ();
			this.Enabled = false;
		}

		private void m_frmWaitCD_OnCancel(object sender, EventArgs e)
		{
			// This handler is called when WaitCD form's Cancel button is
			// clicked. Make sure to abort.
			// 
			m_bAborted = true;
			m_nero.Abort ();
		}

		private void m_nero_OnNonEmptyCDRW(ref NERO_RESPONSE Response)
		{
			// This event will be fired only if the proper conditions
			// are met AND if NERO_BURN_FLAGS.NERO_BURN_FLAG_DETECT_NON_EMPTY_CDRW
			// was specified to the burn fucntion. Ask if we should erase
			// the rewritable.
			// 
			if (DialogResult.Yes != MessageBox.Show (this, "The rewritable disc is not empty. It needs to be erased in order to continue. Do you want to erase the disc?", "Erase?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				// If the answer was NO, ask for a new disc.
				// 
				Response = NERO_RESPONSE.NERO_RETURN_RESTART;
			}
			else
			{
				// If YES, then do erase the disc and continue burning
				// afterwards.
				// 
				EraseProgressForm frm = new EraseProgressForm (m_drive, true, false);
				frm.ShowDialog (this);

				Response = NERO_RESPONSE.NERO_RETURN_CONTINUE;
			}
		}

		private void m_drive_OnDisableAbort(ref bool EnableAbort)
		{
			// This event will be fired at appropriate moments
			// only if NERO_BURN_FLAGS.NERO_BURN_FLAG_DISABLE_ABORT
			// was specified to the burning function. Make sure to
			// update the state of the Cancel button accordingly.
			// 
			c_Cancel.Enabled = EnableAbort;
		}

		// This function burns a copy of a disc in source drive.
		// 
		public void BurnDiscCopy (NeroDrive sourceDrive, bool bOnTheFly, bool bDeleteImage, int iReadSpeed, int iNumberOfRetries, bool bIgnoreDataReadErrors, bool bIgnoreAudioReadErrors, bool bReadISRCAndMediaCatalogNumber, bool bRAWReadMode, NERO_BURN_FLAGS burnFlags, int iWriteSpeed, NERO_MEDIA_TYPE mediaType, System.Windows.Forms.IWin32Window owner)
		{
            string image;
            MessageBox.Show("Waehlen sie einen Ort aus, an dem das Image gespeichert werden soll.");
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = ".iso";
            dialog.Filter = "ISO Files (*.iso)|*.iso";
            if (dialog.ShowDialog() == DialogResult.Cancel)
            {
                DialogResult = DialogResult.Cancel;
                this.Close();
                return;
            }
            image = dialog.FileName;
            ImagePath = image;

			try
			{
				// Subscribe to events and then pass all parameters to
				// the burning function.
				// 
				SubscribeToEvents (true);

				m_drive.BurnDiscCopy (sourceDrive,
					bOnTheFly,
					image,
					bDeleteImage,
					iReadSpeed,
					iNumberOfRetries,
					bIgnoreDataReadErrors,
					bIgnoreAudioReadErrors,
					bReadISRCAndMediaCatalogNumber,
					bRAWReadMode,
					burnFlags,
					iWriteSpeed,
					mediaType);

				this.ShowDialog (owner);
			}
			catch (COMException ex)
			{
				SubscribeToEvents (false);

				MessageBox.Show (this, ex.Message);
			}
		}

		private void c_SaveLog_Click(object sender, System.EventArgs e)
		{
			// Ask for a filename to save to and then write the whole
			// log to the file.
			// 
			SaveFileDialog dlg = new SaveFileDialog ();
			dlg.DefaultExt = ".log";
			dlg.Filter = "Log files (*.log)|*.log|All files (*.*)|*.*";

			if (DialogResult.OK == dlg.ShowDialog (this))
			{
				System.IO.StreamWriter fs = new System.IO.StreamWriter (dlg.FileName);
				fs.Write (m_nero.ErrorLog);
			}
		}
	}
}
