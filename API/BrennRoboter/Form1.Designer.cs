﻿namespace BrennRoboter
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.initializeButton = new System.Windows.Forms.Button();
            this.aufnehmenButton = new System.Windows.Forms.Button();
            this.ablegenButton = new System.Windows.Forms.Button();
            this.spindelNachBrennerNutton = new System.Windows.Forms.Button();
            this.brennerNachSpindelButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // initializeButton
            // 
            this.initializeButton.Location = new System.Drawing.Point(12, 12);
            this.initializeButton.Name = "initializeButton";
            this.initializeButton.Size = new System.Drawing.Size(268, 31);
            this.initializeButton.TabIndex = 0;
            this.initializeButton.Text = "Initialize";
            this.initializeButton.UseVisualStyleBackColor = true;
            this.initializeButton.Click += new System.EventHandler(this.initializeButton_Click);
            // 
            // aufnehmenButton
            // 
            this.aufnehmenButton.Location = new System.Drawing.Point(12, 49);
            this.aufnehmenButton.Name = "aufnehmenButton";
            this.aufnehmenButton.Size = new System.Drawing.Size(268, 31);
            this.aufnehmenButton.TabIndex = 1;
            this.aufnehmenButton.Text = "Aufnehmen";
            this.aufnehmenButton.UseVisualStyleBackColor = true;
            this.aufnehmenButton.Click += new System.EventHandler(this.aufnehmenButton_Click);
            // 
            // ablegenButton
            // 
            this.ablegenButton.Location = new System.Drawing.Point(12, 86);
            this.ablegenButton.Name = "ablegenButton";
            this.ablegenButton.Size = new System.Drawing.Size(268, 31);
            this.ablegenButton.TabIndex = 2;
            this.ablegenButton.Text = "Ablegen";
            this.ablegenButton.UseVisualStyleBackColor = true;
            this.ablegenButton.Click += new System.EventHandler(this.ablegenButton_Click);
            // 
            // spindelNachBrennerNutton
            // 
            this.spindelNachBrennerNutton.Location = new System.Drawing.Point(12, 123);
            this.spindelNachBrennerNutton.Name = "spindelNachBrennerNutton";
            this.spindelNachBrennerNutton.Size = new System.Drawing.Size(268, 31);
            this.spindelNachBrennerNutton.TabIndex = 3;
            this.spindelNachBrennerNutton.Text = "Spindel nach Brenner";
            this.spindelNachBrennerNutton.UseVisualStyleBackColor = true;
            this.spindelNachBrennerNutton.Click += new System.EventHandler(this.spindelNachBrennerNutton_Click);
            // 
            // brennerNachSpindelButton
            // 
            this.brennerNachSpindelButton.Location = new System.Drawing.Point(12, 160);
            this.brennerNachSpindelButton.Name = "brennerNachSpindelButton";
            this.brennerNachSpindelButton.Size = new System.Drawing.Size(268, 31);
            this.brennerNachSpindelButton.TabIndex = 4;
            this.brennerNachSpindelButton.Text = "Brenner nach Spindel";
            this.brennerNachSpindelButton.UseVisualStyleBackColor = true;
            this.brennerNachSpindelButton.Click += new System.EventHandler(this.brennerNachSpindelButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 293);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(268, 48);
            this.button1.TabIndex = 5;
            this.button1.Text = "BrennerTest";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 353);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.brennerNachSpindelButton);
            this.Controls.Add(this.spindelNachBrennerNutton);
            this.Controls.Add(this.ablegenButton);
            this.Controls.Add(this.aufnehmenButton);
            this.Controls.Add(this.initializeButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button initializeButton;
        private System.Windows.Forms.Button aufnehmenButton;
        private System.Windows.Forms.Button ablegenButton;
        private System.Windows.Forms.Button spindelNachBrennerNutton;
        private System.Windows.Forms.Button brennerNachSpindelButton;
        private System.Windows.Forms.Button button1;

    }
}

