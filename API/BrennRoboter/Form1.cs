﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BurnControl;
using RobotControl;
using NEROLib;

namespace BrennRoboter
{
    public partial class Form1 : Form
    {
        IRobotControl _robotController;

        public Form1()
        {
            InitializeComponent();

            _robotController = new RobotController("COM1");
        }

        private void initializeButton_Click(object sender, EventArgs e)
        {
            _robotController.Initialize();
        }

        private void aufnehmenButton_Click(object sender, EventArgs e)
        {
            _robotController.Aufnehmen();
        }

        private void ablegenButton_Click(object sender, EventArgs e)
        {
            _robotController.Ablegen();
        }

        private void spindelNachBrennerNutton_Click(object sender, EventArgs e)
        {
            _robotController.SpindelNachBrenner();
        }

        private void brennerNachSpindelButton_Click(object sender, EventArgs e)
        {
            _robotController.BrennerNachSpindel();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //BurnRobot.BurnRobotController robotController = new BurnRobot.BurnRobotController("COM1");

            //robotController.AddBurnTask(new BurnTask(BurnControllerFactory.Factory.GetBurner(BurnControllerEnum.ImageBurner), 1));
            //robotController.InitBurn();
            //robotController.Burn();
        }
    }
}
