﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotControl
{
    /// <summary>
    /// Die Antworten, die der Roboter zurückgeben kann
    /// </summary>
    public enum Reply
    {
        InvalidCommand = 0x8B,
        CommandExecuted = 0x8A,

        DiskVonEingangsSpindelInBrennerGelegt = 0xA0,
        DiskVonBrennerZurZielspindelTransportiert = 0xA1,
        DiskAufgenommen = 0xA2,
        Bereit = 0xAA,
        DiskAbgelegt = 0xA3,

        KeineDiskImBrenner = 0x82,
        KopierstationstuerOffen = 0x87,
        EingangsspindelLeer = 0x88,
        KeineAusgangsspindel = 0x8F,
    }
    /// <summary>
    /// Bietet Methoden um Replys zu überprüfen (z.B. IsError) oder IsValidReply
    /// </summary>
    public static class ReplyHelper
    {
        public static bool IsError(Reply reply)
        {
            return reply == Reply.EingangsspindelLeer || reply == Reply.InvalidCommand
                            || reply == Reply.KeineAusgangsspindel || reply == Reply.KeineDiskImBrenner
                            || reply == Reply.KopierstationstuerOffen;
        }
        public static bool IsValidReply(byte data)
        {
            return Enum.IsDefined(typeof(Reply), (Reply)data);
        }
    }
}