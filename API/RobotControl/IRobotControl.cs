﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotControl
{
    public interface IRobotControl
    {
        bool Working { get; }
        Status CurrentStatus { get; }
        Reply LastError { get; }

        bool Initialize();
        bool Aufnehmen(); //Hebt eine Disk auf
        bool Ablegen(); //Legt eine Disk ab
        bool SpindelNachBrenner(); //Hebt eine Disk auf -> bewegt sie zum Brenner -> ablegen
        bool BrennerNachSpindel(); //Hebt die Disk vom Brenner auf -> bewegt sie zur Ablagespindel -> ablegen

        void Dispose();
    }
}