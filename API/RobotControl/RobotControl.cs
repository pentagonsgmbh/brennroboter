﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace RobotControl
{
    public class RobotController : IDisposable, IRobotControl
    {
        public bool Working
        {
            get
            {
                if (CurrentStatus == Status.Error || CurrentStatus == Status.Ready)
                    return false;
                return true;
            }
        }
        public Status CurrentStatus { get; private set; }
        /// <summary>
        /// Gibt den letzten Fehler zurueck, oder null, wenn der letzte Befehl erfolgreich war.
        /// </summary>
        public Reply LastError
        {
            get
            {
                return _lastError;
            }
        }

        /// <summary>
        /// Der COM-Port zur Steuerung des Roboters
        /// </summary>
        SerialPort _serialPort;
        /// <summary>
        /// Die letzte Antwort die vom Roboter empfangen wurde
        /// </summary>
        Reply _lastError;
        /// <summary>
        /// Das letzte Command, das dem Roboter geschickt wurde.
        /// </summary>
        Command _lastCommand;

        public RobotController(string portName)
        {
            _serialPort = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
            _serialPort.ReadTimeout = 1000 * 60; // 1 Minute warten bevor eine Timeout Exception auftritt
            CurrentStatus = Status.Ready;
        }

        public void Dispose()
        {
        }

        /// <summary>
        /// Schickt dem Roboter einen Befehl.
        /// z.B. Arm zur Spindel bewegen etc.
        /// </summary>
        /// <param name="command"></param>
        private void SendCommand(Command command)
        {
            byte data;
            try
            {
                _serialPort.Open();
                byte[] cmd = { (byte)command, };
                _serialPort.Write(cmd, 0, 1);
                byte cmdAns = (byte)_serialPort.ReadByte();
                // Warten bis die Antwort gesendet wird
                byte execAns = (byte)_serialPort.ReadByte();
                _serialPort.Close();

                data = execAns;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }

            //Hier wird überprüft, ob die Antwort in dem Reply-Enum vorhanden ist.
            //Und sichergestellt, dass data in Reply gecastet werden kann.
            if (!ReplyHelper.IsValidReply(data))
                throw new UnknownReplyException(data);

            Reply lastReply = (Reply)data;

            //Falls der zuletzt gesendete Befehl fertig ist, den Status auf ready setzen
            if (lastReply == Reply.InvalidCommand)
            {
                throw new InvalidCommandException(_lastCommand);
            }
            else if (ReplyHelper.IsError(lastReply))
            {
                CurrentStatus = Status.Error;
                _lastError = lastReply;
            }
            else if ((CurrentStatus == Status.Aufnehmen && lastReply == Reply.DiskAufgenommen)
                   || (CurrentStatus == Status.Ablegen && lastReply == Reply.DiskAbgelegt)
                   || (CurrentStatus == Status.SpindelNachBrenner && lastReply == Reply.DiskVonEingangsSpindelInBrennerGelegt)
                   || (CurrentStatus == Status.BrennerNachSpindel && lastReply == Reply.DiskVonBrennerZurZielspindelTransportiert))
            {
                CurrentStatus = Status.Ready;
            }
        }
        /// <summary>
        /// Überprüft, ob die Ports offen sind, ob der Roboter gerade arbeitet. --> Exception wenn er das tut (sollte eigentlich nicht passieren)
        /// Schickt dem Roboter den Befehl und wartet auf Ausführung, gibt danach zurück, ob das erfolgreich war.
        /// </summary>
        /// <param name="command"></param>
        private bool ExecuteCommand(Command command)
        {
            //Überprüfen, ob der Roboter bereit ist
            CheckReady();
            //Den aktuellen Status setzen
            CurrentStatus = StatusHelper.CommandToStatus(command);
            //Den letzten ausgeführten Befehl merken
            _lastCommand = command;
            //Dem Roboter den Befehl schicken
            SendCommand(command);
            //Zurückgeben, ob der Befehl erfolgreich ausgeführt wurde.
            // (Wenn der Roboter einen Fehler meldet wird CurrentStatus auf Status.Error gesetzt)
            return CurrentStatus == Status.Ready;
        }

        /// <summary>
        /// Überprüft, ob der Roboter bereit ist und der SerialPort geöffnet ist
        /// </summary>
        private void CheckReady()
        {
            if (Working)
                throw new WorkingException();
        }

        //=============ROBOTER-FUNKTIONEN==============================
        /// <summary>
        /// Setzt den Roboter auf Ausgangstellung (Arm, Error Status, ...)
        /// </summary>
        /// <returns>Ob das Initialisieren erfolgreich war</returns>
        public bool Initialize()
        {
            return ExecuteCommand(Command.Initialize);
        }
        /// <summary>
        /// Hebt eine Disk auf
        /// </summary>
        /// <returns></returns>
        public bool Aufnehmen()
        {
            bool success = ExecuteCommand(Command.Aufnehmen);

            //Wenn Aufnehmen nicht ausgeführt werden konnte wieder Initialisieren
            if (!success)
                Initialize();

            return success;
        }
        /// <summary>
        /// Legt eine Disk ab
        /// </summary>
        /// <returns></returns>
        public bool Ablegen()
        {
            bool success = ExecuteCommand(Command.Ablegen);

            //Wenn Command nicht ausgeführt werden konnte wieder Initialisieren
            if (!success)
                Initialize();

            return success;
        }
        /// <summary>
        /// Bewegt den Arm zu der Rohling-Spindel, hebt die CD auf.
        /// Bewegt dann den Arm zum Brenner und laesst die CD fallen.
        /// </summary>
        /// <returns></returns>
        public bool SpindelNachBrenner()
        {
            bool success = ExecuteCommand(Command.SpindelNachBrenner);

            //Wenn Command nicht ausgeführt werden konnte wieder Initialisieren
            if (!success)
                Initialize();

            return success;
        }
        /// <summary>
        /// Hebt die CD auf, bewegt den Arm vom Brenner zur AblageSpindel und
        /// laesst die CD fallen.
        /// </summary>
        /// <returns></returns>
        public bool BrennerNachSpindel()
        {
            bool success = ExecuteCommand(Command.BrennerNachSpindel);

            //Wenn Command nicht ausgeführt werden konnte wieder Initialisieren
            if (!success)
                Initialize();

            return success;
        }
    }
}