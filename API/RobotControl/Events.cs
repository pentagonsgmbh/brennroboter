﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotControl
{
    public delegate void FortschrittUpdatedEventHandler(object sender, FortschrittUpdatedEventArgs e);

    public class FortschrittUpdatedEventArgs : EventArgs
    {
        public string NewFortschritt { get; private set; }

        public FortschrittUpdatedEventArgs(string newFortschritt)
        {
            NewFortschritt = newFortschritt;
        }
    }
}