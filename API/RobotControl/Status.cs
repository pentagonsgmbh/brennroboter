﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotControl
{
    /// <summary>
    /// Stellt den aktuellen Status des Roboters dar
    /// </summary>
    public enum Status
    {
        Ready,                     //Der Roboter ist bereit
        Aufnehmen,                 //Der Roboter nimmt gerade eine CD auf
        Ablegen,                   //Der Roboter legt gerade eine CD ab
        SpindelNachBrenner,        //Der Roboter bewegt sich gerade von der ersten Spindel zum Brenner
        BrennerNachSpindel,        //Der Roboter bewegt sich gerade von dem Brenner zur zweiten Spindel
        Error,                     //Der Roboter hat gerade einen Fehler festgestellt
    }
    public static class StatusHelper
    {
        /// <summary>
        /// Gibt den Status zurueck, den ein bestimmtes Command ausloest.
        /// z.B. Command 'Aufnehmen' loest den Status Status.Aufnehmen aus.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static Status CommandToStatus(Command command)
        {
            Status status = Status.Ready;
            switch (command)
            {
                case Command.Ablegen:
                    status = Status.Ablegen;
                    break;
                case Command.Aufnehmen:
                    status = Status.Aufnehmen;
                    break;
                case Command.BrennerNachSpindel:
                    status = Status.BrennerNachSpindel;
                    break;
                case Command.SpindelNachBrenner:
                    status = Status.SpindelNachBrenner;
                    break;
                case Command.Initialize:
                    status = Status.Ready;
                    break;
            }
            return status;
        }
    }
}
