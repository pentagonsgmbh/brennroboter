﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotControl
{
    abstract class SteuerungsException : Exception
    {
        public SteuerungsException(string message)
            : base(message)
        {
        }
    }
    /// <summary>
    /// Wird aufgerufen, wenn der SerialPort geschlossen ist
    /// </summary>
    class ComPortClosedException : SteuerungsException
    {
        public ComPortClosedException()
            : base("Der COM-Port ist nicht offen")
        {
        }
    }
    /// <summary>
    /// Wird aufgerufen, wenn der Roboter gerade am arbeiten ist.
    /// </summary>
    class WorkingException : SteuerungsException
    {
        public WorkingException()
            : base("Der Roboter arbeitet gerade noch")
        {
        }
    }
    /// <summary>
    /// Wird aufgerufen, wenn der Roboter eine unbekannte Antwort zurückgibt
    /// </summary>
    class UnknownReplyException : SteuerungsException
    {
        public UnknownReplyException(byte reply)
            : base("Unbekannte Antwort des Roboters empfangen: " + reply.ToString())
        {
        }
    }
    /// <summary>
    /// Wird aufgerufen, wenn der Roboter einen ihm unbekannten Befehl meldet
    /// </summary>
    class InvalidCommandException : SteuerungsException
    {
        public InvalidCommandException(Command command)
            : base("Der Roboter hat gemeldet, dass ihm ein Unbekannter Befehl gesendet wurde: " + command.ToString())
        {
        }
    }
    class CommandHasNoStatusException : Exception
    {
        public CommandHasNoStatusException(Command command)
            : base("Es wurde versucht ein Command in einen Status umzuwandeln, dass keinen Status hat: " + command.ToString())
        {
        }
    }
}
