﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotControl
{
    public enum Command
    {
        Initialize = 0xAA,
        Aufnehmen = 0xA2,
        Ablegen = 0xA3,
        SpindelNachBrenner = 0xA0,       //Bewegt den Arm von der Eingangsspindel zum Brenner
        BrennerNachSpindel = 0xA1,        //Bewegt den Arm vom Brenner zur Ausgangsspindel
    }
}
